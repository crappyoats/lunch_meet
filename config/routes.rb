Rails.application.routes.draw do
  root           'pages#home'
  get 'signup' => 'users#new'
  get 'about' => 'pages#about'
  get 'contact' => 'pages#contact'
  get 'what_is_lunch_meet' => 'pages#what_is_lunch_meet'
  get 'coldcuts'  => 'users#online'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

 resources :users do
 resources :conversations, :controller => "user_conversations"
 member do
 post :signin
 end
 collection do
 post :signout
 end
 end

 resources :conversations, :controller => "user_conversations" do
 resources :messages
 member do
 post :mark_as_read
 post :mark_as_unread
 end
 end
end

