require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(username: "Dildo Baggins", email: "dbagg@gmail.com",
                    password: "dickknobs", password_confirmation: "dickknobs")
  end
  
  test "should be valid user" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.username = "      "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end
  
  test "username length" do
    @user.username = "a" * 21
    assert_not @user.valid?
  end
  
  test "email length" do
    @user.email = "a" * 100 + "@gmail.com"
    assert_not @user.valid?
  end
  
  test "valid emails" do
    valid_emails = ["crappyoats@gmail.com", "CrAppY@gmail.com", "CRAPPY-OATS@GMAIL.COM","crapp+y@G.mail.com"]
      valid_emails.each do |valid|
        @user.email = valid
        assert @user.valid?, "#{valid.inspect} should be valid"
      end
    end
    
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com crappyoats@gmail...com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end
  
    test "email addresses should be unique" do
    duplicate_user = @user.dup
    @user.save
    assert_not duplicate_user.valid?
  end
  
  test "password length" do
    @user.password = "a" * 5
    assert_not @user.valid?
  end
  
  test "password too long" do
    @user.password = "a" * 51
    assert_not @user.valid?
  end

end
